{ lib, config, ... }:

with lib;
let cfg = config.nxc.configs.nix;
in {
  options = {
    nxc.configs.nix.enable = mkEnableOption "Nix configuration";
  };

  config = mkIf cfg.enable {
    nix = {
      settings.trusted-public-keys = [
        "cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY="
        "myrkr:J6fof5Sgzj9i0uOWtJCg5ID7fSRe7beL5ddlSreQxfw="
      ];

      # distribute builds to odroid (more powerful)
      # distributedBuilds = true;
      # buildMachines = [ {
      #   hostName = "odroid";
      #   system = "aarch64-linux";
      #   settings.max-jobs = 6;
      #   speedFactor = 2;
      #   supportedFeatures = [ ];
      #   mandatoryFeatures = [ ];
      # }];
    };
  };
}
