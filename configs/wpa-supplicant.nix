{ lib, config, creds, ... }:

with lib;
let cfg = config.nxc.configs.wpa-supplicant;
in {
  options = {
    nxc.configs.wpa-supplicant.enable = mkEnableOption "Wifi configuration with wpa_supplicant";
  };

  config = mkIf cfg.enable {
    networking = {
      wireless = {
        enable = true;
        userControlled = {
          enable = true;
          group = "wheel";
        };
        networks = {
          tatooine = {
            pskRaw = creds.ssid.tatooine;
          };
          AndroidAP-tony = {
            pskRaw = creds.ssid.AndroidAP-tony;
          };
          AndroidAP-chris = {
            pskRaw = creds.ssid.AndroidAP-chris;
          };
        };
      };
    };
  };
}
