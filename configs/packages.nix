{ lib, config, pkgs, ... }:

with lib;
let cfg = config.nxc.configs.packages;
in {
  options = {
    nxc.configs.packages.enable = mkEnableOption "Packages configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      emacs-nox nano git tmux rlwrap
      sshfs autojump
      mg pavucontrol
      gnome.eog
    ];
  };
}
