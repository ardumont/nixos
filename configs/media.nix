{ lib, config, pkgs, creds, ... }:

with lib;
let cfg = config.nxc.configs.media;
in {
  options = {
    nxc.configs.media.enable = mkEnableOption "Media tools";
  };

  config = mkIf cfg.enable {
    environment = {
      systemPackages = with pkgs; [
        vlc
        gimp-with-plugins
     ];
    };
  };
}
