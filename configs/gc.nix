{ lib, config, ... }:

with lib;
let cfg = config.nxc.configs.gc;
in {
  options = {
    nxc.configs.gc.enable = mkEnableOption "Nix configuration";
  };

  config = mkIf cfg.enable {
    nix = {
      gc = {
        automatic = true;
        dates = "weekly";
        options = "--delete-older-than 30d";
      };

      extraOptions = ''
        min-free = ${toString (100 * 1024 * 1024)}
        max-free = ${toString (1024 * 1024 * 1024)}
      '';
    };
  };
}
