{ lib, config, creds, ... }:

with lib;
let cfg = config.nxc.configs.build-concurrency-light;
in {
  options = {
    nxc.configs.build-concurrency-light.enable = mkEnableOption "Build concurrency light";
  };

  config = mkIf cfg.enable {
    # aarch64 servers can be high load when compiling so decrease a bit
    nix.settings.max-jobs = mkDefault 1;
  };
}
