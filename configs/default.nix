{ lib, config, pkgs, creds, networks, storage, pytools, lib-helper, media, system,
  pkgs-ard-mediatomb, hostname, mypkgs, identity, ... }:

{
  imports = map (
    m: import m {
      inherit
        lib config pkgs creds networks storage pytools lib-helper
        media system pkgs-ard-mediatomb hostname mypkgs identity;
    } ) [
      ./laptop.nix
      ./laptop-lid-close.nix
      ./sound.nix
      ./xserver-default.nix
      ./printer.nix
      ./wpa-supplicant.nix
      ./udev.nix
      ./xserver-basic.nix
      ./mounts-client.nix
      ./dnsmasq-client.nix
      ./dns-server.nix
      ./minidlna.nix
      ./redis.nix
      ./syncthing.nix
      ./weechat.nix
      ./nfs-server.nix
      ./mounts-server.nix
      ./aliases.nix
      ./headless.nix
      ./build-concurrency-light.nix
      ./swap.nix
      ./packages.nix
      ./admin-tools.nix
      ./users.nix
      ./nix.nix
      ./openssh.nix
      ./tmux.nix
      ./transmission.nix
      ./pytools-services.nix
      ./mediatomb
      ./pytools-convert.nix
      ./wireguard-server.nix
      ./wireguard-client.nix
      ./network-manager.nix
      ./mobile.nix
      ./metrics-server.nix
      ./metrics-exporter.nix
      ./xserver-phosh.nix
      ./zfs.nix
      ./gc.nix
      ./nix-allow-broken.nix
      ./wine.nix
      ./media.nix
    ];
}
