{ lib, config, networks, ... }:

with lib;
let cfg = config.nxc.configs.metrics-server;
in {
  options = {
    nxc.configs.metrics-server.enable = mkEnableOption "Metrics";
  };

  config = mkIf cfg.enable {
    # grafana configuration
    services = {
      grafana = {
        enable = true;
        settings = {
          server = {
            domain = "grafana.lan";
            http_port = 80;
            http_addr = networks.machines.grafana.ipAddress;
          };
        };
      };
      prometheus = {
        enable = true;
        port = 9090;
      };
    };
  };
}
