{ config, lib, pkgs, media, storage, creds, ... }:

let serviceUser = "flexget";
    home = "/var/lib/${serviceUser}";
    root-dir = storage.tv-shows;
    # jinja template names to clarify the configuration below
    tvshow-first-letter = "{{ tvdb_series_name|lower|first }}";
    tvshow-name = "{{ tvdb_series_name|lower|replace(' ','-') }}";
    season = "s{{ tvdb_season }}";
    season-padded = "{{ tvdb_season | pad(2) }}";
    episode-padded = "{{ tvdb_episode | pad(2) }}";
    season-episode = "s${season-padded}e${episode-padded}";
    episode-title = "{{ tvdb_ep_name | default('missing-title') | lower | replace(' ', '-') }}";
    # build yaml out of the media entries
    yaml-tvshows = lib.concatMapStrings (
      title: let
        tv-show = lib.getAttr title media;
        tvdb-id = lib.getAttr "tvdb-id" tv-show;
        title-entry-line = "      - \"" + title + "\":\n";
        set-entry-line = "          set:\n";
        tvdb-id-line = "            tvdb_id: " + builtins.toString tvdb-id + "\n";
      in title-entry-line + set-entry-line + tvdb-id-line
    ) (lib.attrNames media);
    # creds.rss: Set[str, str]
    # yaml str
    schedule-entries = let
      make-sched-entry = title: "  - tasks:\n    - ${title}\n    schedule:\n      hour: \"*\"\n";
      in lib.concatMapStrings make-sched-entry (lib.attrNames creds.rss);
    # yaml str
    task-entries = let
      make-task-entry = title: rss: "  ${title}:\n    rss: ${rss}\n    template:\n      - tv\n";
      in lib.concatMapStrings lib.id (lib.mapAttrsToList make-task-entry creds.rss);
in
{
  # the service expects the user to exist
  users.extraUsers.flexget = {
    name = serviceUser;
    group = "flexget";
    extraGroups = [ "media" ];
    inherit home;
    createHome = true;
    useDefaultShell = true;
  };

  services.flexget = {
    enable = true;
    user = serviceUser;
    homeDir = home;
    systemScheduler = false;  # defined in the flexget config below
    config = ''
schedules:
'' + schedule-entries + ''
  - tasks:
    - seed_series_db
    schedule:
      month: '*'

tasks:
'' + task-entries + ''
  seed_series_db:
    filesystem:
      regexp: .*(avi|mkv|mp4)$
      path: ${root-dir}
      recursive: yes
      retrieve: files
    template:
      - tv

templates:
  transmission_instance:
    transmission:
      host: ${creds.transmission.host}
      port: ${creds.transmission.port}
      username: ${creds.transmission.web-username}
      password: ${creds.transmission.web-password}
      main_file_ratio: ${creds.transmission.ratio}
      main_file_only: yes
      rename_like_files: yes
      include_subs: yes
      skip_files:
        - '*.nfo'
        - '*.sfv'
        - '*[sS]ample*'
        - '*.txt'
        - '*.exe'
      ratio: ${creds.transmission.ratio}

  tv:
    template:
      - transmission_instance
    thetvdb_lookup: yes
    tmdb_lookup: yes
    exists_series:
      - ${root-dir}/${tvshow-first-letter}/${tvshow-name}
    set:
      path: "${root-dir}/${tvshow-first-letter}/${tvshow-name}/s${season-padded}/"
      content_filename: "${tvshow-name}-${season-episode}-${episode-title}"
    series:
'' + yaml-tvshows;
  };
}
