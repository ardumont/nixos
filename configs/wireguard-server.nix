{ lib, config, pkgs, creds, networks, ... }:

let cfg = config.nxc.configs.wireguard-server;
    listen-port = creds.wireguard.server.port;
    wg-interface = "wg0";
    eth-interface = "eth0";
in {
  options = {
    nxc.configs.wireguard-server.enable = lib.mkEnableOption "Wireguard configuration";
  };

  config = lib.mkIf cfg.enable {
    networking = {
      nat = {
        enable = true;
        externalInterface = eth-interface;
        internalInterfaces = [ wg-interface ];
      };
      firewall = {
        allowedUDPPorts = [ listen-port ];
      };

      wireguard.interfaces =
        let iptables = "${pkgs.iptables}/bin/iptables";

        in {
          # "wg0" is the network interface name
          "${wg-interface}" = {
            # Determines the IP address and subnet of the server's end of the tunnel interface
            ips = [ "${networks.ips.vlan.odroid}/24" ];

            # The port that Wireguard listens to. Must be accessible by the client
            listenPort = listen-port;

            # allows wireguard server to route your traffic to the internet
            postSetup = ''
            # Setting up NAT firewall rules
            ${iptables} -t nat -I POSTROUTING 1 -o ${eth-interface} -j MASQUERADE
            # Accept all traffic created by wg0 interface
            ${iptables} -I INPUT 1 -i ${wg-interface} -j ACCEPT
            # Allow packets to be routed through the WireGuard server
            ${iptables} -I FORWARD 1 -i ${eth-interface} -o ${wg-interface} -j ACCEPT
            ${iptables} -I FORWARD 1 -i ${wg-interface} -o ${eth-interface} -j ACCEPT
            # Open udp port
            ${iptables} -I INPUT 1 -i ${eth-interface} -p udp --dport ${toString listen-port} -j ACCEPT
          '';

            # Undo the rules
            postShutdown = ''
            ${iptables} -D INPUT -i ${eth-interface} -p udp --dport ${toString listen-port} -j ACCEPT
            ${iptables} -D FORWARD -i ${wg-interface} -o ${eth-interface} -j ACCEPT
            ${iptables} -D FORWARD -i ${eth-interface} -o ${wg-interface} -j ACCEPT
            ${iptables} -D INPUT -i ${wg-interface} -j ACCEPT
            ${iptables} -t nat -D POSTROUTING -o ${eth-interface} -j MASQUERADE
          '';

            # Could use private key file instead
            privateKey = creds.wireguard.server.private-key;

            # List of allowed peers.
            peers = [
              {
                publicKey = creds.wireguard.clients.yavin4.public-key;
                # List of peer IPs assigned within the tunnel subnet
                allowedIPs = [ "${networks.ips.vlan.yavin4}/32" ];
              }
              {
                publicKey = creds.wireguard.clients.bespin.public-key;
                allowedIPs = [ "${networks.ips.vlan.bespin}/32" ];
              }
              {
                publicKey = creds.wireguard.clients.myrkr.public-key;
                allowedIPs = [ "${networks.ips.vlan.myrkr}/32" ];
              }
              {
                publicKey = creds.wireguard.clients.alderaan.public-key;
                allowedIPs = [ "${networks.ips.vlan.alderaan}/32" ];
              }
              {
                publicKey = creds.wireguard.clients.dathomir.public-key;
                allowedIPs = [ "${networks.ips.vlan.dathomir}/32" ];
              }
            ];
          };
        };
    };
  };
}
