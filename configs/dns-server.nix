{ lib, config, networks, ... }:

with lib;
let cfg = config.nxc.configs.dns-server;
    # to-host-entry: (str -> str -> str -> bool) -> str
    to-host-entry = ip: name: domain-name: alias:
      let str-alias = if alias then " ${name}" else "";
      in "${ip} ${name}.${domain-name}${str-alias}";
    # to-host-entries: (Set[str, str] -> str -> bool) -> Set[str, str]
    to-host-entries = set-ips: domain-name: alias:
      lib.mapAttrs (name: ip:
        if ip == null then null else to-host-entry ip name domain-name alias
      ) set-ips;
    # concat: Set[str, str] -> str
    concat = ips:
      lib.concatMapStrings (s: s + "\n") (builtins.filter (x: x != null) (lib.attrValues ips));
    # lan-ips: List[str]
    lan-ips = to-host-entries networks.ips.lan networks.lan true;
    # vlan-ips: List[str]
    vlan-ips = to-host-entries networks.ips.vlan networks.vlan false;
in {
  options = {
    nxc.configs.dns-server.enable = mkEnableOption "DNS server configuration";
  };

  config = mkIf cfg.enable {
    services.dnsmasq = {
      enable = true;
      alwaysKeepRunning = true;
    };

    networking = {
      firewall = {
        allowedTCPPorts = [ 53 ];
        allowedUDPPorts = [ 53 ] ;
      };

      extraHosts = "# ${networks.lan}" + "\n" + (concat lan-ips) +
                   "\n" +
                   "# ${networks.vlan}" + "\n" + (concat vlan-ips);
    };
  };
}
