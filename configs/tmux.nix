{ lib, config, pkgs, ... }:

with lib;
let cfg = config.nxc.configs.tmux;
in {
  options = {
    nxc.configs.tmux.enable = mkEnableOption "Tmux configuration";
  };

  config = mkIf cfg.enable {

    # we don't use the tmux server part because it conflicts with
    # the weechat tmux session
    environment = {
      systemPackages = with pkgs; [
        xclip tmux
      ];

      etc."tmux.conf".text =
        let xclip = "${pkgs.xclip}/bin/xclip";
            tmux = "${pkgs.tmux}/bin/tmux";
        in ''
unbind C-b
# changing default prefix to CTRL+q
set -g prefix C-q

# Set key mode
set -gw mode-keys emacs
set -gw status-keys emacs
# Do not modify titles
set -g set-titles off

# reload configuration
bind r source-file /etc/tmux.conf \; display-message "tmux setup reloaded..."
# split window horizontally
bind | split-window -h
# split window vertically
bind - split-window -v

# No escape time between the prefix command and the following type
set -s escape-time 0
# Copy and paste configuration
bind C-w run "${tmux} show-buffer | ${xclip} -i -selection clipboard"
# Copy CLIPBOARD to tmux paste buffer and paste tmux paste buffer
bind C-y run "${tmux} set-buffer -- \"$(${xclip} -o -selection clipboard)\"; ${tmux} paste-buffer"

# go to the last window <prefix> C-a
bind-key C-a last-window

set -g status-interval 2

# start windows index at 1
set -g base-index 1
# start panes index at 1
setw -g pane-base-index 1
# UTF8 options
set-option -g default-terminal "screen-256color"

# history limit
set -g history-limit 8196
'';
    };
  };
}
