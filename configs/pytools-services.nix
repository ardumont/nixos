{ lib, config, pkgs, pytools, mypkgs, media, storage, creds, lib-helper, system, ... }:

let cfg = config.nxc.configs.pytools-services;
    mnamer = mypkgs.packages."${system}".mnamer;
    pytools-package = pytools.defaultPackage."${system}";
    python-with-tools = pkgs.python3.withPackages (
      pps: [ pytools-package ]
    );
    pytools-bin = "${pytools-package}/bin/ardumont-pytools";
    dramatiq-config = ''
dramatiq:
  broker:
    type: redis
    host: ${creds.redis.host}
    port: ${toString creds.redis.port}
          '';
    sort-media-config = dramatiq-config + ''

          application-command:
            config-path: /etc/mnamer/mnamer.json
            command: ${mnamer}/bin/mnamer
            tvshow-id-filename: .tvshow-id

          movie:
            destination: ${storage.movies-sas}
          tvshow:
            destination: ${storage.tv-shows}
          '';
    media-json = "${storage.ardumont.config}/media.json";
in {
  options = {
    nxc.configs.pytools-services.enable = lib.mkEnableOption "Pytools services configuration";
  };

  config = lib.mkIf cfg.enable {
    environment = {
      systemPackages = [ pytools mnamer ];

      etc = {
        "mnamer/mnamer.json".source = ./mnamer.json;
        # default configuration for most services
        "ardumont/dramatiq.yml".text = dramatiq-config;
        # except for sort-media which has a tad more
        "ardumont/sort-media.yml".text = sort-media-config;
        "ardumont/media.json".text = builtins.toJSON media;
      };
    };

    # service to listen to new media files in camera folder (<~ syncthing)
    systemd.services.sort-media = lib-helper.define-systemd-service {
      description = "Sort media files depending on their nature (movies, tv-shows)";
      config-path = "${storage.ardumont.config}/sort-media.yml";
      command = ''${python-with-tools.interpreter} -m dramatiq \
                     ardumont.pytools.dramatiq.app \
                     ardumont.pytools.tasks.sort_media \
                     --queues sort rename-media rename-tvshow rename-movie \
                     --processes 1 \
                     --threads 1
    '';
    };

    systemd.services.rename-media = lib-helper.define-systemd-service {
      description = "Basic heuristic service to rename media files";
      command = ''${python-with-tools.interpreter} -m dramatiq \
                     ardumont.pytools.dramatiq.app \
                     ardumont.pytools.tasks.basic_rename_media \
                     ardumont.pytools.tasks.rename_media \
                     --queues rename-tvshow rename-media rename-metadata rename-video \
                     --processes 1 \
                     --threads 1
    '';
    };

    systemd.services.cleanup-media = lib-helper.define-systemd-service {
      description = "Clean up media files";
      command = ''${python-with-tools.interpreter} -m dramatiq \
        ardumont.pytools.dramatiq.app \
        ardumont.pytools.tasks.cleanup \
        --queues cleanup \
        --processes 1 \
        --threads 1
        '';
    };

    systemd.services.rotate-media = lib-helper.define-systemd-service {
      description = "Rotate media files";
      command = ''${python-with-tools.interpreter} -m dramatiq \
                     ardumont.pytools.dramatiq.app \
                     ardumont.pytools.tasks.rotate_video \
                     --queues rotate \
                     --processes 1 \
                     --threads 1
    '';
    };

    # service to listen to new media files in camera folder (<~ syncthing)
    systemd.services.watcher-camera = lib-helper.define-systemd-service {
      description = "Watch camera media folder for new media files";
      command = ''${pytools-bin} \
                    watch rename-media-event \
                    --path ${storage.camera} \
                    --cleanup
    '';
    };

    systemd.services.watcher-camera-input = lib-helper.define-systemd-service {
      description = "Watch camera media folder for new media files to rotate";
      command = ''${pytools-bin} \
                     watch convert-video-event \
                     --path ${storage.camera-input}
      '';
    };

    # service to listen to new media files to rotate (because wrong orientation)
    systemd.services.watcher-rotate-video = lib-helper.define-systemd-service {
      description = "Rotate video folders watcher";
      command = ''${pytools-bin} \
                   watch rotate-video-event \
                   --path ${storage.camera-rotate-90} \
                   --path ${storage.camera-rotate-180} \
                   --path ${storage.camera-rotate-270} \
                   --destination-path ${storage.camera}
      '';
    };

    # # service to listen to new media files to rotate (because wrong orientation)
    systemd.services.watcher-convert-video = lib-helper.define-systemd-service {
      description = "convert video watcher";
      command = ''${pytools-bin} \
                     watch convert-video-event \
                     --video --recurse --no-keep-metadata \
                     --path ${storage.tv-shows} \
                     --path ${storage.movies}
      '';
      wants = [ "volume-share.mount" ] ;
    };

    services.cron =
      let root-path = storage.tv-shows;
          init-media-fs =
            pkgs.writeShellScriptBin "init-media-fs" ''
              ${pytools-bin} tvshow init-from-json \
              --media-json ${media-json} \
              --root-path ${root-path}
         '';
      in {
        enable = true;
        systemCronJobs = [
          "5 0 * * *  tony ${init-media-fs}"  # 5 minutes after midnight daily
        ];
      };
  };
}
