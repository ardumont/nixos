{ lib, config, pkgs, networks, ... }:

with lib;
let cfg = config.nxc.configs.printer;
in {
  options = {
    nxc.configs.printer.enable = mkEnableOption "Printer specific configuration";
  };

  config = mkIf cfg.enable {

    # https://nixos.wiki/wiki/Printing
    services.printing = {
      enable = true;
      drivers = [
        pkgs.epson-escpr
      ];
    };

    hardware = {
      sane = {
        enable = true;
        # extraBackends = [ pkgs.hplipWithPlugin ]; # no need because activated previously
      };

      # once cups is installed
      # lpinfo -m | grep -i epson-et-2820
      # epson-inkjet-printer-escpr/Epson-ET-2820_Series-epson-escpr-en.ppd EPSON ET-2820 Series , Epson Inkjet Printer Driver (ESC/P-R) for Linux

      printers.ensurePrinters = [
        {
          name = "epson-et2821";
          description = "Epson ET-2821";
          deviceUri = "lpd://${networks.machines.epson-et2821.ipAddress}:515/PASSTHRU";
          model = "epson-inkjet-printer-escpr/Epson-ET-2820_Series-epson-escpr-en.ppd";
        }
      ];
      # ./epson-inkjet-printer-escpr-1.7.20/ppd/Epson-ET-2820_Series-epson-escpr-en.ppd
    };
  };
}
