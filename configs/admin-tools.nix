{ lib, config, pkgs, ... }:

with lib;
let cfg = config.nxc.configs.admin-tools;
in {
  options = {
    nxc.configs.admin-tools.enable = mkEnableOption "Admin tools configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      git tmux
      which file wget curl tree htop dstat
      ncdu lshw usbutils lsof coreutils pciutils
      unixtools.netstat traceroute tcpdump iftop nethogs
      pciutils mount rlwrap
      zlib libzip p7zip zip unzip pigz gnutar pv
      parted testdisk
      binutils
      dmidecode
      nmap netcat ngrep tcpdump
      acpi acpid acpitool
      pmutils
      rlwrap jq
    ];
  };
}
