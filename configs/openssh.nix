{ lib, config, ... }:

with lib;
let cfg = config.nxc.configs.openssh;
in {
  options = {
    nxc.configs.openssh.enable = mkEnableOption "Openssh configuration";
  };

  config = mkIf cfg.enable {
    services.openssh = {
      enable = true;
      settings.PasswordAuthentication = false;
      openFirewall = true;
      extraConfig = ''
# To allow ~/.ssh/authorized_keys to be linked on /nix/store (world
# readable) on clients...
StrictModes no
      '';
    };
  };
}
