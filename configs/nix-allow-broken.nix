{ lib, config, ... }:

with lib;
let cfg = config.nxc.configs.nix-allow-broken;
in {
  options = {
    nxc.configs.nix-allow-broken.enable = mkEnableOption "Nix allow broken configuration";
  };

  config = mkIf cfg.enable {
    nixpkgs.config.allowBroken = true;
  };
}
