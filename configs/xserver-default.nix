{ config, lib, pkgs, ... }:

with lib;
let cfg = config.nxc.configs.xserver-default;
in {
  options = {
    nxc.configs.xserver-default.enable = mkEnableOption "Basic xserver configuration";
  };

  config = mkIf cfg.enable {

    services.xserver = {
      enable = true;
      exportConfiguration = true; # create link /etc/X11/xorg.conf to real conf

      # keyboard
      layout = "fr";
      xkbOptions = "eurosign:e,terminate=ctrl_alt_backspace";

      displayManager = {
        defaultSession = "xfce";
        lightdm.enable = true;
      };

      desktopManager = {
        xfce.enable = true;
        xterm.enable = true;
      };
    };

    programs.thunar.plugins = with pkgs.xfce; [ thunar-archive-plugin ];

    environment.systemPackages = with pkgs; [
      firefox pinta xscreensaver
      evince gnome3.file-roller chromium
    ];
  };
}
