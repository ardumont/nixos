{ ... }:

{
  virtualisation = {
    # No vbox
    virtualbox.host.enable = false;
    # libvirt
    libvirtd.enable = true;
    # Activate docker service
    docker = {
      enable = true;
      # storageDriver = "devicemapper";
    };
  };
}
