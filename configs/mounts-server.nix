{ lib, config, storage, ... }:

with lib;
let cfg = config.nxc.configs.mounts-server;
in {
  options = {
    nxc.configs.mounts-server.enable = mkEnableOption "Specific volume mounts for server";
  };

  config = mkIf cfg.enable {
    fileSystems."${storage.volume}" = {
      device = "/dev/disk/by-label/usbstorage";
      fsType = "ext4";
      options = [ "rw" "nosuid" "nodev" "noexec" "relatime"
                  "errors=remount-ro" "data=ordered" "user" "nofail" ];
    };

    system.activationScripts.media =
      ''
      mkdir -m 0775 -p ${storage.volume}
      chown root:users ${storage.volume}
      ( [ -d ${storage.downloads} ] && chown transmission:media ${storage.downloads} ) || echo ""
      '';
  };
}
