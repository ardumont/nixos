{ lib, config, pkgs, creds, ... }:

with lib;
let cfg = config.nxc.configs.users;
    defaultUser = "tony";
    load_keys = (f:
      builtins.filter (e: e != [] && e != "")
        (builtins.split "\n" "${builtins.readFile f}"));
    default_keys = load_keys ../data/pubkeys/default.nix;
in {
  options = {
    nxc.configs.users.enable = mkEnableOption "Base users configuration";
  };

  config = mkIf cfg.enable {
    programs = {
      zsh.enable = true;
      bash.enableCompletion = true; # for nix-shell
    };

    users = {
      defaultUserShell = "${pkgs.zsh}/bin/zsh";
      mutableUsers = false;
      groups = {
        git = {
          gid = 1002;
        };
        media = {
          gid = 1003;
        };
        flexget = {
          gid = 1004;
        };
      };
      users = {
        tony = {
          description = "Antoine R. Dumont (@ardumont)";
          uid = 1000;
          group = "users";
          createHome = true;
          home = "/home/${defaultUser}";
          extraGroups = [ "wheel" "audio" "video" "media" "dialout" "docker" "libvirtd" ];
          useDefaultShell = true;
          openssh.authorizedKeys.keys =
            builtins.concatLists [ default_keys (load_keys ../data/pubkeys/tony.nix) ];
          hashedPassword = creds.identity.ardumont.hashed-passwd;
          isNormalUser = true;
          isSystemUser = false;
        };
        chris = {
          description = "Christelle B. Héritier";
          uid = 1001;
          group = "users";
          createHome = true;
          home = "/home/chris";
          extraGroups = [ "wheel" "audio" "video" "media" ];
          useDefaultShell = true;
          openssh.authorizedKeys.keys =
            builtins.concatLists [ default_keys (load_keys ../data/pubkeys/chris.nix) ];
          hashedPassword = creds.identity.cheritier.hashed-passwd;
          isNormalUser = true;
          isSystemUser = false;
        };
        git = {
          uid = 1002;
          group = "git";
          openssh.authorizedKeys.keys = default_keys;
          createHome = true;
          home = "/home/git";
          extraGroups = [ "git" ];
          useDefaultShell = true;
          shell = "${pkgs.bash}/bin/bash";
          hashedPassword = creds.identity.git.hashed-passwd;
          isNormalUser = false;
          isSystemUser = true;
        };
        root = {
          openssh.authorizedKeys.keys = default_keys;
          hashedPassword = creds.identity.root.hashed-passwd;
        };
        kids = {
          description = "Kids";
          uid = 1003;
          group = "users";
          createHome = true;
          home = "/home/kids";
          extraGroups = [ "audio" "video" ];
          useDefaultShell = true;
          openssh.authorizedKeys.keys =
            builtins.concatLists [ default_keys (load_keys ../data/pubkeys/tony.nix) ];
          hashedPassword = creds.identity.kids.hashed-passwd;
          isNormalUser = true;
          isSystemUser = false;
        };
      };
    };
  };
}
