{ lib, config, pkgs, ... }:

with lib;
let cfg = config.nxc.configs.wine;
in {
  options = {
    nxc.configs.wine.enable = mkEnableOption "Wine configuration";
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      wineWowPackages.unstable
      grapejuice
    ];
  };
}
