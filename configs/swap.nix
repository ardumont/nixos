{ lib, config, creds, ... }:

with lib;
let cfg = config.nxc.configs.swap;
in {
  options = {
    nxc.configs.swap.enable = mkEnableOption "Activate swap";
  };

  config = mkIf cfg.enable {
    # strongly recommended
    swapDevices = [ { device = "/dev/disk/by-label/swap"; } ];
  };
}
