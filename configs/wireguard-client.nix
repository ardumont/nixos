{ lib, config, pkgs, creds, networks, hostname, ... }:

let cfg = config.nxc.configs.wireguard-client;
in {
  options = {
    nxc.configs.wireguard-client.enable = lib.mkEnableOption "Wireguard client configuration";
  };

  config = lib.mkIf cfg.enable {
    # Enable Wireguard client
    networking = {
      # not configured for ipv6
      enableIPv6 = false;

      wireguard.interfaces =
        let client-ip = networks.ips.vlan."${hostname}";
            server-remote-dns = creds.wireguard.server.remote-dns;
            server-remote-port = creds.wireguard.server.remote-port;
            client-private-key = creds.wireguard.clients."${hostname}".private-key;
            server-public-key = creds.wireguard.server.public-key;
        in {
          # "wg0" is the network interface name. You can name the interface arbitrarily.
          wg0 = {
            # Determines the IP address and subnet of the client's end of the tunnel interface.
            ips = [ "${client-ip}/24" ];

            # One should use privateKeyFile
            privateKey = client-private-key;

            # For a client configuration, one peer entry for the server will suffice.
            peers = [
              {
                # Public key of the server (not a file path).
                publicKey = server-public-key;

                # Forward all the traffic via VPN.
                # allowedIPs = [ "0.0.0.0/0" ];

                # Or forward only particular subnets
                allowedIPs = [ "10.8.9.0/24" ];

                # Set this to the server IP and port.
                endpoint = "${server-remote-dns}:${toString server-remote-port}";

                # Send keepalives every 25 seconds. Important to keep NAT tables alive.
                persistentKeepalive = 25;
              }
            ];
          };
        };
    };
  };
}
