{ lib, config, creds, pkgs, ... }:

with lib;
let cfg = config.nxc.configs.udev;
    home = "/home/tony";
in {
  options = {
    nxc.configs.udev.enable = mkEnableOption "Udev specific configuration";
  };

  config = mkIf cfg.enable {
    services.udev = {
      extraRules = with pkgs; ''
# oneplus phones
SUBSYSTEM=="usb", ATTRS{idVendor}=="18d1", ATTRS{idProduct}=="4ee7", MODE="0660", GROUP="plugdev", SYMLINK+="oneplus3"
SUBSYSTEM=="usb", ATTRS{idVendor}=="05c6", ATTRS{idProduct}=="9001", MODE="0660", GROUP="plugdev", SYMLINK+="oneplus2"
# odroid go
SUBSYSTEM=="tty", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", MODE="0660", GROUP="plugdev", SYMLINK+="odroidgo"
# pinephone
SUBSYSTEM=="usb", ATTRS{idVendor}=="1a86", ATTRS{idProduct}=="7523", MODE="0660", GROUP="plugdev", SYMLINK+="pinephone"

# my keyboard
SUBSYSTEM=="usb", ATTRS{idVendor}=="045e", ATTRS{idProduct}=="07a5", MODE="0660", GROUP="plugdev", SYMLINK+="my-keyboard", ENV{DISPLAY}=":0", ENV{XAUTHORITY}="${home}/.Xauthority", RUN+="${xorg.setxkbmap}/bin/setxkbmap -layout us -model pc105 -option eurosign:e -option ctrl:nocaps -option terminate=ctrl_alt_backspace -option altwin:meta_alt -option compose:lwin"
# the one below does not work, needs to be installed alongside nix rules it seems
# KERNEL=="card0", SUBSYSTEM=="drm", ENV{DISPLAY}=":0", ENV{XAUTHORITY}="${home}/.Xauthority", RUN+="${home}/bin/connect-or-reset-screens"
    '' ;
    };
  };
}
