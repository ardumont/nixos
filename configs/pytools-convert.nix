{ lib, config, pkgs, creds, pytools, storage, lib-helper, system,... }:

with lib;
let cfg = config.nxc.configs.pytools-convert;
    pytools-package = pytools.defaultPackage."${system}";
    python-with-tools = pkgs.python3.withPackages (
      pps: [ pytools-package ]
    );
in {
  options = {
    nxc.configs.pytools-convert.enable = mkEnableOption "pytools-convert configuration";
  };

  config = mkIf cfg.enable {
    environment = {
      systemPackages = [ pytools-package ];

      # default configuration for most services
      etc."ardumont/dramatiq.yml".text = ''
dramatiq:
  broker:
    type: redis
    host: ${creds.redis.host}
    port: ${toString creds.redis.port}

avconv:
  command: ${pkgs.libav}/bin/avconv
'';
    };

    systemd.services.convert-video = lib-helper.define-systemd-service {
      description = "Convert video queue consumption";
      command = ''${python-with-tools.interpreter} -m dramatiq \
                     ardumont.pytools.dramatiq.app \
                     ardumont.pytools.tasks.convert_video \
                     --queues convert \
                     --processes 1 \
                     --threads 1
    '';
      wants = [ "volume-share.mount" ];
      user = "tony";
    };
  };
}
