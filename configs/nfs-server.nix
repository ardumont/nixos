{ lib, config, creds, storage, networks, ... }:

with lib;
let cfg = config.nxc.configs.nfs-server;
    machine = networks.machines.myrkr.ipAddress;
in {
  options = {
    nxc.configs.nfs-server.enable = mkEnableOption "NFS server configuration";
  };

  config = mkIf cfg.enable {

    fileSystems."/export${storage.share}" = {
      device = storage.share;
      options = [ "bind" ];
    };

    services.nfs.server = {
      enable = true;
      exports = ''
/export${storage.share} ${machine}(rw,nohide,insecure,no_subtree_check)
'';
    };

    networking.firewall.allowedTCPPorts = [ creds.nfs.port ];
  };
}
