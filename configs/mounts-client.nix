{ lib, config, storage, networks, ... }:

with lib;
let cfg = config.nxc.configs.mounts-client;
    server = networks.machines.odroid.hostName;
    domain = networks.lan;
in {
  options = {
    nxc.configs.mounts-client.enable = mkEnableOption "Specific client mount points configuration";
  };

  config = mkIf cfg.enable {
    fileSystems."${storage.share}" = {
      device = "${server}.${domain}:/export${storage.share}";
      fsType = "nfs";
      options = [
        "nofail" "noauto" "noatime" "users" "x-systemd.automount"
      ];
    };
  };
}
