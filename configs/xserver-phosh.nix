{ lib, config, creds, pkgs, ... }:

with lib;
let cfg = config.nxc.configs.xserver-phosh;
in {
  options = {
    nxc.configs.xserver-phosh.enable = mkEnableOption "X server mobile configuration";
  };

  config = mkIf cfg.enable {
    hardware.opengl.enable = lib.mkDefault true;
    fonts.enableDefaultFonts = lib.mkDefault true;
    # FIXME: something is off when importing nixpkgs...
    # This should find it but it does not
    # programs.phosh-ui.enable = true;
    programs.dconf.enable = true;
    services.dbus.packages = [ pkgs.gnome3.dconf ];
    services.dbus.enable = true;
    # services.fwupd.enable = true;
    # programs.light.enable = true;

    # gnome3 toggles it on and complains, no need for it, disabling
    networking.networkmanager.enable = false;

    services.xserver = {
      enable = true;
      # exportConfiguration = true; # create link /etc/X11/xorg.conf to real conf

      # keyboard
      layout = "us";
      xkbVariant = "euro";

      # touchpad
      libinput = {
        enable = true;
        touchpad.tapping = true;
      };

      displayManager = let phosh-session-name = "sm.puri.Phosh"; in {
        sessionPackages = [ pkgs.phosh ];
        sessionData.sessionNames = [ phosh-session-name ];
        defaultSession = phosh-session-name;
        gdm.enable = true;

        autoLogin = {
          enable = true;
          user = "tony";
        };
      };
      desktopManager.gnome.enable = true;
    };
    environment = {
      etc."phosh/phoc.ini" = {
        text = ''
[output:WAYLAND0]
mode = 720x1440
scale = 2
      '';

        # The UNIX file mode bits
        mode = "0444";
      };
      systemPackages = with pkgs; [
        gnome3.gnome-session
        gnome3.gnome-desktop
        wget emacs-nox tmux
      ];
    };
  };
}
