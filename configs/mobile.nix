{ config, lib, ... }:

with lib;
let cfg = config.nxc.configs.mobile;
in {
  options = {
    nxc.configs.mobile.enable = mkEnableOption "Mobile configuration";
  };

  config = mkIf cfg.enable {
    powerManagement.cpuFreqGovernor = lib.mkDefault "ondemand";
  };
}
