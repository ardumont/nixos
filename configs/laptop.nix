{ config, lib, ... }:

with lib;
let cfg = config.nxc.configs.laptop;
in {
  options = {
    nxc.configs.laptop.enable = mkEnableOption "Laptop specific configuration";
  };

  config = mkIf cfg.enable {
    powerManagement.cpuFreqGovernor = mkDefault "powersave";
  };
}
