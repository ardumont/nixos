{ config, lib, ... }:

with lib;
let cfg = config.nxc.configs.laptop-lid-close;
    defaultPolicy = "hibernate";
in {
  options = {
    nxc.configs.laptop-lid-close.enable = mkEnableOption "Laptop lid close specific configuration";
  };

  config = mkIf cfg.enable {
    services.logind.lidSwitch = defaultPolicy;
    services.logind.lidSwitchDocked = defaultPolicy;
  };
}
