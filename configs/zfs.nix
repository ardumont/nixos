{ lib, config, ... }:

let cfg = config.nxc.configs.zfs;
in {
  options = {
    nxc.configs.zfs = {
      enable = lib.mkEnableOption "ZFS configuration";
    };
  };

  config = lib.mkIf cfg.enable {
    services.zfs = {
      trim.enable = true;
      autoScrub.enable = true;

      # autoSnapshot = {
      #   enable = true;
      #   frequent = 8; # keep the latest eight 15-minute snapshots (instead of four)
      #   monthly = 1;  # keep only one monthly snapshot (instead of twelve)
      # };
    };
  };
}
