{ lib, config, creds, ... }:

with lib;
let cfg = config.nxc.configs.redis;
in {
  options = {
    nxc.configs.redis.enable = mkEnableOption "Redis configuration";
  };

  config = mkIf cfg.enable {
    services.redis.servers.local = {
      enable = true;
      bind = "0.0.0.0";          # open on local network
      port = creds.redis.port;   # with port
      databases = 1;             # let's start small
      openFirewall = true;
    };

    users.groups.redis = { };
    users.users.redis = {
      isSystemUser = true;
      group = "redis";
      shell = "/bin/bash";
    };
  };
}
