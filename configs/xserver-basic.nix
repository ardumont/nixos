{ lib, config, creds, ... }:

with lib;
let cfg = config.nxc.configs.xserver-basic;
in {
  options = {
    nxc.configs.xserver-basic.enable = mkEnableOption "Basic X server configuration";
  };

  config = mkIf cfg.enable {
    services.xserver = {
      enable = true;
      exportConfiguration = true; # create link /etc/X11/xorg.conf to real conf

      # keyboard
      layout = "us";
      xkbOptions = "eurosign:e,terminate=ctrl_alt_backspace";

      displayManager = {
        defaultSession = "xterm";
        lightdm.enable = true;
      };

      desktopManager = {
        xterm.enable = true;
      };
    };
  };
}
