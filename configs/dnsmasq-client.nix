{ lib, config, networks, ... }:

with lib;
let cfg = config.nxc.configs.dnsmasq-client;
    path = "NetworkManager/dnsmasq.d";
    lan = networks.lan;
    vlan = networks.vlan;
    server-lan-ip = networks.ips.lan.odroid;
    server-vlan-ip = networks.ips.vlan.odroid;
    work-set-vlan-domain = [ 192 168 100 ];
    work-domain = networks.to-domain work-set-vlan-domain;
    work-vlan-ip = networks.make-ip work-domain 29;
    device = "wg0";
in {
  options = {
    nxc.configs.dnsmasq-client.enable = mkEnableOption "Dnsmasq client configuration";
  };

  config = mkIf cfg.enable {
    services.dnsmasq = {
      enable = true;
      resolveLocalQueries = true;
      alwaysKeepRunning = true;
      settings = {
        # Add some cache
        cache-size ="1000";
        # Fallback .lan or .vlan queries to vlan dns
        server = [
          "/.${lan}/${server-vlan-ip}@${device}"
          "/${networks.domains.lan-reverse}.in-addr.arpa/${server-vlan-ip}@${device}"
          "/.${vlan}/${server-vlan-ip}@${device}"
          "/${networks.domains.vlan-reverse}.in-addr.arpa/${server-vlan-ip}@${device}"
          # Fallback work dns queries to work dns
          "/internal.softwareheritage.org/${work-vlan-ip}"
          "/internal.staging.swh.network/${work-vlan-ip}"
          "/100.168.192.in-addr.arpa/${work-vlan-ip}"
          "/101.168.192.in-addr.arpa/${work-vlan-ip}"
          "/200.168.192.in-addr.arpa/${work-vlan-ip}"
          "/201.168.192.in-addr.arpa/${work-vlan-ip}"
          "/202.168.192.in-addr.arpa/${work-vlan-ip}"
          "/203.168.192.in-addr.arpa/${work-vlan-ip}"
          "/204.168.192.in-addr.arpa/${work-vlan-ip}"
          "/205.168.192.in-addr.arpa/${work-vlan-ip}"
          "/206.168.192.in-addr.arpa/${work-vlan-ip}"
          "/207.168.192.in-addr.arpa/${work-vlan-ip}"
        ];
      };

#       extraConfig = ''

# cache-size=1000
# # Fallback .lan or .vlan queries to vlan dns
# server=/.${lan}/${server-vlan-ip}@${device}
# server=/${networks.domains.lan-reverse}.in-addr.arpa/${server-vlan-ip}@${device}
# server=/.${vlan}/${server-vlan-ip}@${device}
# server=/${networks.domains.vlan-reverse}.in-addr.arpa/${server-vlan-ip}@${device}
# # Fallback work dns queries to work dns
# server=/internal.softwareheritage.org/${work-vlan-ip}
# server=/internal.staging.swh.network/${work-vlan-ip}
# server=/100.168.192.in-addr.arpa/${work-vlan-ip}
# server=/101.168.192.in-addr.arpa/${work-vlan-ip}
# server=/200.168.192.in-addr.arpa/${work-vlan-ip}
# server=/201.168.192.in-addr.arpa/${work-vlan-ip}
# server=/202.168.192.in-addr.arpa/${work-vlan-ip}
# server=/203.168.192.in-addr.arpa/${work-vlan-ip}
# server=/204.168.192.in-addr.arpa/${work-vlan-ip}
# server=/205.168.192.in-addr.arpa/${work-vlan-ip}
# server=/206.168.192.in-addr.arpa/${work-vlan-ip}
# server=/207.168.192.in-addr.arpa/${work-vlan-ip}
#       '';
    };
  };
}
