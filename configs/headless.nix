{ lib, config, creds, ... }:

with lib;
let cfg = config.nxc.configs.headless;
in {
  options = {
    nxc.configs.headless.enable = mkEnableOption "Headless profile";
  };

  config = mkIf cfg.enable {
    services.xserver.enable = false;
  };
}
