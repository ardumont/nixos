{ lib, lib-helper, config, pkgs, pytools, storage, creds, networks, system, ... }:

with lib;
let cfg = config.nxc.configs.transmission;
    port = creds.transmission.port;
    peer-port = 54213;
in {
  options = {
    nxc.configs.transmission.enable = mkEnableOption "Transmission specific configuration";
  };

  config = mkIf cfg.enable {
    services.transmission = let
      torrent-done =
        with pkgs;
        let watched-folder = storage.downloads;
            noisy-extensions = "*.exe *.sub *.srt *.txt *.nfo *.jpg *.idx *.part sample.*";
        in writeShellScriptBin "torrent-done" ''
CHOWN=${coreutils}/bin/chown
RM=${coreutils}/bin/rm
RMDIR=${coreutils}/bin/rmdir
MV=${coreutils}/bin/mv
FIND=${findutils}/bin/find

pushd ${watched-folder}

# give the right permission (needs a sudo user without pass)
$CHOWN -Rv tony:media *

# clean up noisy files
for noisy_extension in ${noisy-extensions}; do
  $RM -fv $noisy_extension
  $RM -fv */$noisy_extension
  $RM -fv */*/$noisy_extension
  $RM -fv */*/*/$noisy_extension
done

# clean up old and supposedly empty folder
$FIND -type d -empty -exec $RMDIR {} \;

# move any subfolder's content to the current
$MV -fv */* .
# clean up noisy files if some passed the previous cleanup
for noisy_extension in ${noisy-extensions}; do
  $RM -fv $noisy_extension
done

# clean up old and supposedly empty folder
$FIND -type d -empty -exec $RM -rfv {} \;


popd
    '';
    in {
      enable = true;
      group = "media";
      settings = {
        blocklist-enabled = true;
        blocklist-url = "http://john.bitsurge.net/public/biglist.p2p.gz";
        cache-size-mb = 4;
        dht-enabled = true;
        download-dir = "${storage.downloads}";
        download-limit = 100;
        download-limit-enabled = 0;
        download-queue-enabled = true;
        download-queue-size = 3;
        encryption = 1;
        idle-seeding-limit = 30;
        idle-seeding-limit-enabled = false;
        incomplete-dir-enabled = true;
        incomplete-dir = "${storage.downloads-tmp}";
        inherit peer-port;
        peer-port-random-high = peer-port;
        peer-port-random-low = peer-port;
        peer-port-random-on-start = false;
        peer-limit-global= 100;
        peer-limit-per-torrent = 20;
        port-forwarding-enabled = true;
        ratio-limit-enabled = true;
        ratio-limit = 0.1;
        rename-partial-files = true;
        rpc-authentication-required = true;
        rpc-bind-address = "0.0.0.0";
        rpc-enabled = true;
        rpc-port = port;
        rpc-url = "/transmission/";
        rpc-username = creds.transmission.rpc-username;
        rpc-password = creds.transmission.rpc-password;
        rpc-whitelist-enabled = true;
        rpc-whitelist = "127.0.0.1;${networks.domains.lan}.*;${networks.domains.vlan}.*";
        scrape-paused-torrents-enabled = true;
        # script-torrent-done-enabled = true;
        # script-torrent-done-filename = "${torrent-done}/bin/torrent-done";
        seed-queue-enabled = false;
        seed-queue-size = 10;
        # speed-limit-down = 200;
        # speed-limit-down-enabled = true;
        speed-limit-up = 100;
        speed-limit-up-enabled = true;
      };
    };

    users.extraUsers.transmission = {
      extraGroups = [ "media" ];
    };

    networking.firewall.allowedTCPPorts = [ port ];
  };
}
