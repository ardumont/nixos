{ lib, config, pkgs, ... }:

with lib;
let cfg = config.nxc.configs.weechat;

in {
  options = {
    nxc.configs.weechat.enable = mkEnableOption "Weechat specific configuration";
  };

  config = mkIf cfg.enable {

    environment.systemPackages = [
      pkgs.weechat
    ];

    # Users of this service must be "enabled-linger"ed as well [1]
    # [1] https://www.mythmon.com/posts/2015-02-15-systemd-weechat.html

    systemd.user.services.weechat = {
      enable = true;
      description = "weechat user session";
      serviceConfig =
        let weechat-session-name = "weechat";
            tmux = "${pkgs.tmux}/bin/tmux";
            weechat = "${pkgs.weechat}/bin/weechat";
        in {
          Type = "oneshot";
          ExecStart = "${tmux} -2 -u new-session -d -s ${weechat-session-name}-$(whoami) ${weechat}";
          ExecStop = "${tmux} kill-session -t ${weechat-session-name}-$(whoami)";
          RemainAfterExit = "yes";
        };
      wantedBy = [ "multi-user.target" ];
      wants = [ "network.target" ];
    };
  };
}
