{ lib, config, creds, ... }:

with lib;
let cfg = config.nxc.configs.aliases;
in {
  options = {
    nxc.configs.aliases.enable = mkEnableOption "Alias configuration";
  };

  config = mkIf cfg.enable {
    environment.shellAliases = {
      ll = "ls -l";
      gst = "git status";
      gco = "git checkout";
      gbr = "git branch";
      gfa = "git fetch --all";
      gf = "git fetch";
      em = "emacsclient -nw";
      emc = "emacsclient --create-frame";
    };
  };
}
