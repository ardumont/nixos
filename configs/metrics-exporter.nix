{ lib, config, networks, ... }:

with lib;
let cfg = config.nxc.configs.metrics-exporter;
    exporter-node-port = 9002;
in {
  options = {
    nxc.configs.metrics-exporter.enable = mkEnableOption "Metrics";
  };

  config = mkIf cfg.enable {
    services.prometheus = {
      exporters = {
        node = {
          enable = true;
          enabledCollectors = [ "systemd" ];
          port = exporter-node-port;
        };
      };

      scrapeConfigs = [
        {
          job_name = "odroid";
          static_configs = [{
            targets = [ "${networks.machines.odroid.ipAddress}:${toString exporter-node-port}" ];
          }];
        }
      ];

    };
  };
}
