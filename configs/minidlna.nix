{ lib, config, storage, creds, ... }:

with lib;
let cfg = config.nxc.configs.minidlna;
in {
  options = {
    nxc.configs.minidlna.enable = mkEnableOption "Minidlna specific configuration";
  };

  config = mkIf cfg.enable {
    services.minidlna = {
      enable = true;
      settings = {
        media_dir = [
          "PV,${storage.pictures}"
          "V,${storage.movies}"
          "V,${storage.tv-shows}"
          "V,${storage.courses}"
          "A,${storage.audio}"
          "V,${storage.musics}"
          "V,${storage.judo}"
          "V,${storage.downloads}"
        ];
        friendly_name   = "nas";
        root_container  = "B";
        inotify         = "yes";
        notify_interval = 60;
        wide_links      = "yes";
      };
    };

    users.extraUsers.minidlna = {
      extraGroups = [ "media" ];
    };

    networking.firewall.allowedTCPPorts = [ creds.minidlna.port ];
  };
}
