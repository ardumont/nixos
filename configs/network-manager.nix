{ lib, config, creds, ... }:

with lib;
let cfg = config.nxc.configs.network-manager;
in {
  options = {
    nxc.configs.network-manager.enable = mkEnableOption "Network-manager configuration";
  };

  config = mkIf cfg.enable {
    networking = {
      # nm, nmcli, etc... incompatible with networking.wireless
      # also for a user to manager wireless network, add it to
      # networkmanager group
      networkmanager = {
        enable = true;
      };
    };
  };
}
