{ config, lib, ... }:

with lib;
let cfg = config.nxc.configs.sound;
in {
  options = {
    nxc.configs.sound.enable = mkEnableOption "Sound specific configuration";
  };

  config = mkIf cfg.enable {
    sound = {
      enable = true;
      mediaKeys.enable = true;
    };

    hardware.pulseaudio.enable = true;

    boot.extraModprobeConfig = ''
    options snd slots=snd-hda-intel
    # disable first card and enable the second one
    options snd_hda_intel enable=0,1
      '';
  };
}
