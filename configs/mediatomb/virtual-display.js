/*GRB*
  Gerbera - https://gerbera.io/

  import.js - this file is part of Gerbera.

  Copyright (C) 2018-2020 Gerbera Contributors

  Gerbera is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License version 2
  as published by the Free Software Foundation.

  Gerbera is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Gerbera.  If not, see <http://www.gnu.org/licenses/>.

  $Id$
*/

// This is a simplified version customized to my needs
// with some ugly hacks ¯\_(ツ)_/¯

// doc-add-audio-begin
function addAudio(obj) {
  var desc = '';
  var artist_full;
  var album_full;

  // First we will gather all the metadata that is provided by our
  // object, of course it is possible that some fields are empty -
  // we will have to check that to make sure that we handle this
  // case correctly.
  var title = obj.meta[M_TITLE];

  // Note the difference between obj.title and obj.meta[M_TITLE] -
  // while object.title will originally be set to the file name,
  // obj.meta[M_TITLE] will contain the parsed title - in this
  // particular example the ID3 title of an MP3.
  if (!title) {
    title = obj.title;
  }

  var artist = obj.meta[M_ARTIST];
  if (!artist) {
    artist = 'Unknown';
    artist_full = null;
  } else {
    artist_full = artist;
    desc = artist;
  }

  var album = obj.meta[M_ALBUM];
  if (!album) {
    album = 'Unknown';
    album_full = null;
  } else {
    desc = desc + ', ' + album;
    album_full = album;
  }

  if (desc) {
    desc = desc + ', ';
  }
  desc = desc + title;

  var date = obj.meta[M_DATE];
  if (!date) {
    year = 'Unknown';
  } else {
    year = getYear(date);
    obj.meta[M_UPNP_DATE] = year;
    desc = desc + ', ' + year;
  }

  var genre = obj.meta[M_GENRE];
  if (!genre) {
    genre = 'Unknown';
  } else {
    desc = desc + ', ' + genre;
  }

  var description = obj.meta[M_DESCRIPTION];
  if (!description) {
    obj.description = desc;
  }

  var composer = obj.meta[M_COMPOSER];
  if (!composer) {
    composer = 'None';
  }

  var conductor = obj.meta[M_CONDUCTOR];
  if (!conductor) {
    conductor = 'None';
  }

  var orchestra = obj.meta[M_ORCHESTRA];
  if (!orchestra) {
    orchestra = 'None';
  }

  // uncomment this if you want to have track numbers in front of the title
  // in album view
  var track = obj.meta[M_TRACKNUMBER];
  if (!track) {
    track = '';
  } else {
    if (track.length == 1) {
      track = '0' + track;
    }
    track = track + ' ';
  }

  // let's prefix the release year of the album
  if (year != 'Unknown') {
    album = year + ' - ' + album
  }

  chain = ['audio', artist[0], artist, album];
  obj.title = track + title;
  addCdsObject(obj, createContainerChain(chain), UPNP_CLASS_CONTAINER_MUSIC_ALBUM);
}
// doc-add-audio-end

// doc-add-video-begin
function addVideo(obj) {
  var chain = ['video', 'All Video'];
  addCdsObject(obj, createContainerChain(chain));

  var location = obj.location
  if (location.substr(0, 23) == '/volume/share/pictures/') {  // specific video

    var year = location.substr(23, 4)
    var month = location.substr(28, 2)

    chain = ['photos', year, month];
    addCdsObject(obj, createContainerChain(chain), UPNP_CLASS_CONTAINER);

  } else if (location.substr(0, 23) == '/volume/share/tv-shows/') {// specific tv-shows

    var array_tvshow = getRootPath(object_script_path, location);
    chain = array_tvshow
    addCdsObject(obj, createContainerChain(chain), UPNP_CLASS_CONTAINER);

  } else if (location.substr(0, 21) == '/volume/share/movies/') {// specific movies

    var array_movies = getRootPath(object_script_path, location);
    chain = array_movies
    addCdsObject(obj, createContainerChain(chain), UPNP_CLASS_CONTAINER);

  } else {
    var dir = getRootPath(object_script_path, obj.location);

    if (dir.length > 0) {
      chain = ['video', 'Directories'].concat(dir);
      addCdsObject(obj, createContainerChain(chain));
    }
  }
}
// doc-add-video-end

// doc-add-image-begin
function addImage(obj) {
  var date = obj.meta[M_DATE];
  if (date) {
    var dateParts = date.split('-');
    if (dateParts.length > 1) {
      var year = dateParts[0];
      var month = dateParts[1];

      chain = ['photos', year, month];
      addCdsObject(obj, createContainerChain(chain), UPNP_CLASS_CONTAINER);
    }
  }
}
// doc-add-image-end

// doc-add-trailer-begin
function addTrailer(obj) {
  var chain;

  // First we will add the item to the 'All Trailers' container, so
  // that we get a nice long playlist:

  chain = ['Online Services', 'Apple Trailers', 'All Trailers'];
  addCdsObject(obj, createContainerChain(chain));

  // We also want to sort the trailers by genre, however we need to
  // take some extra care here: the genre property here is a comma
  // separated value list, so one trailer can have several matching
  // genres that will be returned as one string. We will split that
  // string and create individual genre containers.

  var genre = obj.meta[M_GENRE];
  if (genre) {

    // A genre string "Science Fiction, Thriller" will be split to
    // "Science Fiction" and "Thriller" respectively.

    genres = genre.split(', ');
    for (var i = 0; i < genres.length; i++) {
      chain = ['Online Services', 'Apple Trailers', 'Genres', genres[i]];
      addCdsObject(obj, createContainerChain(chain));
    }
  }

  // The release date is offered in a YYYY-MM-DD format, we won't do
  // too much extra checking regading validity, however we only want
  // to group the trailers by year and month:

  var reldate = obj.meta[M_DATE];
  if ((reldate) && (reldate.length >= 7)) {
    chain = ['Online Services', 'Apple Trailers', 'Release Date', reldate.slice(0, 7)];
    addCdsObject(obj, createContainerChain(chain));
  }

  // We also want to group the trailers by the date when they were
  // originally posted, the post date is available via the aux
  // array. Similar to the release date, we will cut off the day and
  // create our containres in the YYYY-MM format.

  var postdate = obj.aux[APPLE_TRAILERS_AUXDATA_POST_DATE];
  if ((postdate) && (postdate.length >= 7)) {
    chain = ['Online Services', 'Apple Trailers', 'Post Date', postdate.slice(0, 7)];
    addCdsObject(obj, createContainerChain(chain));
  }
}
// doc-add-trailer-end

// main script part

if (getPlaylistType(orig.mimetype) === '') {
  var arr = orig.mimetype.split('/');
  var mime = arr[0];

  // All virtual objects are references to objects in the
  // PC-Directory, so make sure to correctly set the reference ID!

  var obj = orig;
  obj.refID = orig.id;

  if (mime === 'audio') {
    addAudio(obj);
  }

  if (mime === 'video') {
    if (obj.onlineservice === ONLINE_SERVICE_APPLE_TRAILERS) {
      addTrailer(obj);
    } else {
      addVideo(obj);
    }
  }

  if (mime === 'image') {
    addImage(obj);
  }

  // We now also have OGG Theora recognition, so we can ensure that
  // Vorbis
  if (orig.mimetype === 'application/ogg') {
    if (orig.theora === 1) {
      addVideo(obj);
    } else {
      addAudio(obj);
    }
  }
}
