{ lib, config, creds, storage, ... }:

with lib;
let cfg = config.nxc.configs.syncthing;
    port = creds.syncthing.port;
in {
  options = {
    nxc.configs.syncthing.enable = mkEnableOption "Syncthing configuration";
  };

  config = mkIf cfg.enable {
    services.syncthing = {
      enable = true;
      guiAddress = "0.0.0.0:${toString port}";
      openDefaultPorts = true;
      # Devices to sync with
      settings = {
        devices = {
          oneplus-nord = {
            id = "33Q2PHW-6IE3FDB-MLUWXHO-OLHDITP-ABGENGK-LOTVPT4-ZSSUSAG-RZY7CA7";
          };
          oneplus-nord-chris = {
            id = "4MK6FNR-6SGECU6-VT2CJFZ-4YGTOR5-QJXIW3P-OPYABKT-F4HDXND-A2JFGQU";
          };
        };
        # solely the following folders will be sync-ed
        folders = {
          "${storage.camera}" = {
            id = "camera";
            devices = [ "oneplus-nord" "oneplus-nord-chris" ];
            type = "receiveonly";
          };
        };
      };
    };
    users.extraUsers.syncthing = {
      extraGroups = [ "media" ];
    };

    networking.firewall.allowedTCPPorts = [ port ];
  };
}
