{ storage, ... }:

{
  # function to define systemd services (mostly the same definition)
  define-systemd-service = {
    description,
    command,
    config-path ? "${storage.ardumont.config}/dramatiq.yml",
    enable ? true,
    kill-mode ? "process",
    user ? "tony",
    group ? "media",
    wants ? [],
  }: {
    inherit enable description;
    serviceConfig = {
      Environment = "ARDUMONT_CONFIG_FILENAME=${config-path}";
      User = user;
      Group = group;
      Type = "simple";
      ExecStart = "${command}";
      KillMode = "${kill-mode}";
      KillSignal = "SIGTERM";
      TimeoutStopSec = 0;
      Restart = "always";
      RestartSec = 10;
    };
    wantedBy = [ "multi-user.target" ];
    wants = [ "network.target" ] ++ wants;
  };
}
