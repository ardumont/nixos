{ config, pkgs, creds, vpn-name, device, cipher,
  with-ta, with-passfile, ... }:

let client = "${config.networking.hostName}";
    path = "/etc/openvpn/keys/${vpn-name}";
    optional-ta = if with-ta then "tls-auth ${path}/ta.key" else "";
    optional-passfile = if with-passfile then "askpass ${path}/private" else "";
    vpn-server = creds.openvpn-client."${vpn-name}".server;
in {
  environment = {
    systemPackages = [ pkgs.openvpn ];

    etc = {
      "openvpn/keys/${vpn-name}/ca.crt".text =
        creds.openvpn-client."${vpn-name}".ca-crt;
      "openvpn/keys/${vpn-name}/${client}.crt".text =
        creds.openvpn-client."${vpn-name}".crt;
      "openvpn/keys/${vpn-name}/${client}.key".text =
        creds.openvpn-client."${vpn-name}".key;
      "openvpn/keys/${vpn-name}/ta.key".text =
        if with-ta then creds.openvpn-client."${vpn-name}".ta else "";
      "openvpn/keys/${vpn-name}/private".text =
        if with-passfile then creds.openvpn-client."${vpn-name}".private else "";
    };
  };

  services.openvpn.servers."${vpn-name}".config = ''
remote ${vpn-server} 1194
client
dev ${device}
proto udp
comp-lzo
resolv-retry infinite
nobind
persist-key
persist-tun
mute-replay-warnings
remote-cert-tls server
key-direction 1
cipher ${cipher}
verb 1
mute 20
user nobody
group nogroup
auth-nocache
log /var/log/openvpn-${vpn-name}.log
status /var/log/openvpn-status-${vpn-name}.log
# this must be installed manually (for now)
ca ${path}/ca.crt
cert ${path}/${client}.crt
key ${path}/${client}.key
${optional-ta}
${optional-passfile}
'';
}
