{ config, lib, pkgs, hostname, home-manager, identity, ... }:

# This module serves the purpose to make home-managed homes work within the
# nixos configuration

let # home-managed logins per hostname
    # Don't know how to set this up more properly
    # This depends mostly from the home implementation in ../home
    hm-logins = {
      "chris" = {
        "*" = "home-chris";
      };
    };
    home-managed-path = login: hostname: (
      let default-home-entry-point = lib.attrByPath [ login "*" ] "" hm-logins;
          home-entry-point = lib.attrByPath [ login hostname ] default-home-entry-point hm-logins;
      in "/home/tony/repo/private/${home-entry-point}/home.nix"
    );
in {
  # Activate home-manager's nixos module
  imports = [ home-manager.nixosModules.home-manager ];

  # Activate home-manager for those logins
  home-manager.users = lib.genAttrs (lib.attrNames hm-logins) (
    login: import ( home-managed-path login hostname ) {
      inherit config lib pkgs identity;
    }
  );
}
