#+title: TODO
#+author: ardumont

* DONE rpi4 [3/3]
CLOSED: [2020-04-11 Sat 19:33]
- [X] Centralize media information (tv-shows)
- [X] Centralize configuration about storage
- [X] Centralize configuration about creds

* DONE Migrate to flake
  CLOSED: [2020-12-13 Sun 19:14]

Pros:
- Faster
- Reproducible
- Buildable: I actually can check on my dev machine it works without actually
  deploying it (even cross architecture \m/)

Cons:
- Still experimental

* DONE Rework to introduce the notion of hosts, role
  CLOSED: [2020-12-13 Sun 19:14]

A machine is defined by its hostname, hardware and architecture. It has one
role. A role is a composition of configs. A config is a composition of modules
called with specific instance of configurations.

Examples:

|-----------+-----------+-----------+--------------+-----------------------+---------------|
| host      | role      | hardware  | architecture | configs               | Note          |
|-----------+-----------+-----------+--------------+-----------------------+---------------|
| yavin4    | developer | laptop    | x86_64       | laptop, xserver, ...  | Not nixos yet |
| alderaan  | teacher   | laptop    | x86_64       | laptop, xserver, ...  |               |
| myrkr     | developer | desktop   | x86_64       | desktop, xserver, ... |               |
| pinephone | mobile    | mobile    | aarch64      | mobile, xserver, ...  |               |
| rpi3      | nas       | rpi       | aarch64      | server, headless, ... |               |
| rpi4      | nas       | rpi       | aarch64      | server, headless, ... |               |
| odroid    | nas       | odroid-n2 | aarch64      | server, headless, ... |               |
|-----------+-----------+-----------+--------------+-----------------------+---------------|

* TODO home-manager
Determine how to declare home-manager with the new architecture and flake.

* TODO pinephone
Fix the apparently mismatch in nixpkgs configuration. It currently does not
find the phosh-ui programs... Although I pinned a specific nixpkgs commit which
contains it and the store path seems to do contain it... ¯\_(ツ)_/¯

* TODO Fix vpn setup
  Eventually migrate out of openvpn.
  Might be time to give a spin to wireguard.
