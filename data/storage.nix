let volume = "/volume";
    transmission = "${volume}/transmission";
    share = "${volume}/share";
in {
  inherit volume transmission share;
  ardumont.config = "/etc/ardumont";
  downloads = "${transmission}/downloads";
  downloads-tmp = "${transmission}/tmp";
  tv-shows = "${share}/tv-shows";
  courses = "${share}/courses";
  judo = "${share}/judo";
  audio = "${share}/audio";
  musics = "${share}/musics";
  pictures = "${share}/pictures";
  movies = "${share}/movies";
  movies-sas = "${share}/movies/sas";
  camera = "${share}/camera";
  camera-input = "${share}/camera/input";
  camera-rotate-90 = "${share}/camera/rotate/90";
  camera-rotate-180 = "${share}/camera/rotate/180";
  camera-rotate-270 = "${share}/camera/rotate/270";
}
