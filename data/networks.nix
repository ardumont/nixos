{ lib, ... }:

let # to-domain: List[int] -> str
    to-domain = domain-set: lib.concatStrings (lib.intersperse "." (map toString domain-set));
    # to-reverse-domain: List[int] -> str
    to-reverse-domain = domain-set: lib.concatStrings (lib.intersperse "." (map toString (lib.reverseList domain-set)));
    # domain-lan-set: List[int]
    domain-lan-set = [ 192 168 150 ];
    # domain-lan: str
    domain-lan = to-domain domain-lan-set;
    # domain-vlan-set: List[int]
    domain-vlan-set = [ 10 8 9 ];
    # domain-vlan: str
    domain-vlan = to-domain domain-vlan-set;
    # make-ip: (str -> int) -> str
    make-ip = domain: ip: domain + "." + (toString ip);
    # lan: str
    lan = "lan";
    # vlan: str
    vlan = "vlan";
    # make-ips: (str -> str -> Set[str, Set[str, Union[int, str]]]) -> Set[str, str]
    make-ips = domain-name: domain: info:
      lib.mapAttrs (name: attr:
        let ip = attr.${domain-name};
        in if (ip == null) then null else make-ip domain ip
      ) info;
    # machine-info: Set[str, Set[str, Union[int, str]]]
    # 12/2023: previous odroid-n2+ (issue with sdcard reader)
    #                            { lan = 43;   vlan = 1;    hwaddr = "00:1e:06:42:fa:06"; };
    machines = let odroid-conf = { lan = 43;   vlan = 1;    hwaddr = "00:1e:06:43:8a:4c"; };
      in {
      nw-address   = { lan = 0;    vlan = null; hwaddr = null; };
      # This laptop died around june/july 2015 after joining swh
      # dagobah      = { lan = 10;   vlan = 3;    hwaddr = "5c:51:4f:2c:e1:24"; };
      corellia     = { lan = 12;   vlan = 2;    hwaddr = "34:02:86:24:7f:5c"; };
      hp8610       = { lan = 13;   vlan = null; hwaddr = "6c:c2:17:17:4a:70"; };
      myrkr        = { lan = 14;   vlan = 5;    hwaddr = "1c:87:2c:b7:35:fe"; };
      rpi          = { lan = 15;   vlan = null; hwaddr = "b8:27:eb:b1:a2:6e"; };
      tv2          = { lan = 16;   vlan = null; hwaddr = "14:49:e0:50:d7:f0"; };
      tv1          = { lan = 17;   vlan = null; hwaddr = "60:6b:bd:b2:46:3a"; };
      alderaan     = { lan = 18;   vlan = 6;    hwaddr = "70:66:55:8a:42:6d"; };
      chris-host   = { lan = 19;   vlan = null; hwaddr = "b8:76:3f:75:b9:e7"; };
      kessel       = { lan = 20;   vlan = null; hwaddr = "8c:53:e6:c8:4e:f9"; };
      hifi         = { lan = 21;   vlan = null; hwaddr = "3c:07:71:e8:ec:3f"; };
      wii          = { lan = 22;   vlan = null; hwaddr = null; };
      wiiu         = { lan = 23;   vlan = null; hwaddr = "18:2a:7b:84:53:ac"; };
      n3ds         = { lan = 25;   vlan = null; hwaddr = "40:d2:8a:30:5d:67"; };
      kids         = { lan = 26;   vlan = null; hwaddr = "ea:e6:67:d9:ec:e9"; };
      rpi3         = { lan = 27;   vlan = 11;   hwaddr = "b8:27:eb:2b:ca:02"; };
      odroid-n2    = { lan = 28;   vlan = null; hwaddr = "00:1e:06:42:0f:3e"; };
      grafana      = odroid-conf;
      ev3dev       = { lan = 29;   vlan = null; hwaddr = "00:13:ef:f4:08:c4"; };
      retropie     = { lan = 30;   vlan = null; hwaddr = "b8:27:eb:9b:d0:19"; };
      mbp-arnaud   = { lan = 31;   vlan = null; hwaddr = "a0:99:9b:15:9d:15"; };
      oneplus2     = { lan = 32;   vlan = null; hwaddr = "c0:ee:fb:92:80:17"; };
      oneplus3     = { lan = 33;   vlan = null; hwaddr = "c0:ee:fb:f9:11:d0"; };
      yavin4       = { lan = 34;   vlan = 10;   hwaddr = "d4:d2:52:b7:36:99"; };
      dathomir     = { lan = 35;   vlan = 8;    hwaddr = "d8:5d:e2:a2:28:d7"; };
      rpi4         = { lan = 36;   vlan = null; hwaddr = "dc:a6:32:43:21:18"; };
      # new librem 14 running as a guix host (tryout)
      # - wired
      dagobah      = { lan = 38;   vlan = null; hwaddr = "00:e0:4c:36:00:11"; };
      # - wifi (needs to settle the macadress somehow)
      # dagobah      = { lan = 38;   vlan = null; hwaddr = "30:10:b3:77:be:5b"; };
      odroid       = odroid-conf;
      talus        = { lan = 45;   vlan = null; hwaddr = "00:23:ae:90:f3:d7"; };
      epson-et2821 = { lan = 46;   vlan = null; hwaddr = "a4:d7:3c:0f:33:d8"; };
      kashyyyk     = { lan = 48;   vlan = null; hwaddr = "cc:6b:1e:82:ad:75"; };
      pinephone    = { lan = 50;   vlan = null; hwaddr = "00:e0:4c:8d:6a:8e"; };
      bespin       = { lan = null; vlan = 7;    hwaddr = null;                };
      freebox      = { lan = 254;  vlan = 4;    hwaddr = null;                };
    };

in {
  inherit make-ip to-domain to-reverse-domain lan vlan;
  # Set[str, str]
  domains = {
    lan = domain-lan;
    lan-reverse = to-reverse-domain domain-lan-set;
    vlan = domain-vlan;
    vlan-reverse = to-reverse-domain domain-vlan-set;
  };
  # Set [str, Set[str, str]]
  ips = {
    lan = make-ips lan domain-lan machines;
    vlan = make-ips vlan domain-vlan machines;
  };
  # Set [str, Set[str, Any]]
  machines = lib.mapAttrs (
    hostname: attr: {
      hostName = hostname;
      ipAddress = make-ip domain-lan attr.lan;
      ethernetAddress = attr.hwaddr;
    }) machines;
}
