{ config, lib, creds, storage, networks, media, rpi4-kernel, ... }:

let nixpkgs = import rpi4-kernel {};  # override to something buildable for rpi4
    lib = nixpkgs.lib;
    config = nixpkgs.config;
    pkgs = nixpkgs.pkgs;
in {
  nxc.roles.server-light.enable = true;

  # for zfs, output of `head -c 8 /etc/machine-id`
  networking.hostId = "647386d8";
  boot = {
    consoleLogLevel = lib.mkDefault 7;
    loader = {
      # NixOS wants to enable GRUB by default
      grub.enable = false;
      raspberryPi = {
        enable = true;
        version = 4;
        # not working yet, initial work [1] closed in favor of [2]
        # [1] https://github.com/NixOS/nixpkgs/pull/70796#issuecomment-552238845
        # [2] https://github.com/NixOS/nixpkgs/pull/97883
        uboot.enable = false;
        # possibility to add some config.txt stanza
        # firmwareConfig = "";
      };
      # Enables the generation of /boot/extlinux/extlinux.conf
      generic-extlinux-compatible.enable = true;
    };
    supportedFilesystems = [ "zfs" ];

    # Raspberry Pi 4
    kernelPackages = pkgs.linuxPackages_rpi4;
    # Issue with fat not being accessible at reboot this fails mounting the
    # /boot partition after that, thus the boot hangs
    # initrd.availableKernelModules = [ "vfat" ];
    # extraModulePackages = with config.boot.kernelPackages; [ vfat ];

    # Needed for the virtual console to work on the RPi 3, as the
    # default of 16M doesn't seem to be enough.  If X.org behaves
    # weirdly, then try increasing this to 256M.
    kernelParams = [
      "cma=32M"                 # graphical issue
      "console=ttyS1,115200n8"  # serial console
    ];

    kernel.sysctl."fs.inotify.max_user_watches" = 1000000;
  };

  # File systems configuration for using the installer's partition layout
  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-label/NIXOS_BOOT";
      fsType = "vfat";
    };
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };
}
