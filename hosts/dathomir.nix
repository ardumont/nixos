{ lib, config, pkgs, nixpkgs, ... }:

{
  system.stateVersion = "21.11";
  nxc.roles.laptop-chris.enable = true;

  nix.settings.max-jobs = lib.mkDefault 1;

  networking.firewall.enable = false;

  # ----- hardware configuration

  # for the wifi broadcom_sta module below
  nixpkgs.config.allowUnfree = true;

  boot = {
    initrd.availableKernelModules = [
      "xhci_hcd" "ehci_pci" "ahci" "usb_storage" "sd_mod" "sr_mod"
    ];
    initrd.kernelModules = [ ];
    # wifi wl module
    extraModulePackages = [ config.boot.kernelPackages.broadcom_sta ];
    # make those modules available at the second stage of boot process
    kernelModules = [ "kvm-intel" "wl" ];

    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
  };

  fileSystems = {
    "/" = { device = "/dev/disk/by-label/nixos"; fsType = "ext4"; };
    "/boot" = { device = "/dev/disk/by-label/boot"; fsType = "vfat"; };
  };

  swapDevices = [ { device = "/dev/disk/by-label/swap"; } ];

  hardware.opengl.extraPackages = [ pkgs.vaapiIntel ];
}
