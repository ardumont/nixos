# odroid-n2+
{ config, lib, pkgs, hostname, home-manager, identity, networks, ... }:

{
  nxc.roles.server.enable = true;
  nxc.configs.swap.enable = false;  # no swap configured
  nix.settings.max-jobs = 4;

  # FIXME: Define that module within a config instead, then enable it for this host
  imports = [
     ../modules/kboot-conf
  ];

  networking = let fbx = networks.ips.lan.freebox;
                   ip = networks.ips.lan.${hostname};
                   submask = 24;
                   nw-address = networks.ips.lan.nw-address;
                   interface = "end0";
  in {
    # head -c 8 /etc/machine-id
    # hostId = "c4e7b98b";
    firewall.enable = false;
    dhcpcd.enable = false;
    useDHCP = false;
    defaultGateway = fbx;
    nameservers = [ "127.0.0.1" fbx "8.8.8.8" ];
    interfaces.${interface} = {
      # installing the right route
      ipv4 = {
        addresses = [{
          address = ip;
          prefixLength = submask;
        }];
        routes = [{
          # Configure the prefix route.
          address = nw-address;
          prefixLength = submask;
          via = fbx;
        }];
      };
    };
  };

  powerManagement.cpuFreqGovernor = lib.mkDefault "ondemand";
  # for python3.9-tvdb_api-3.1.0
  # nixpkgs.config.allowBroken = true;

  hardware.deviceTree.name = "amlogic/meson-g12b-odroid-n2-plus.dtb";
  boot = {
    kernel.sysctl = {
      "net.core.rmem_max" = 4194304;
      "net.core.wmem_max" = 1048576;
      "fs.inotify.max_user_watches" = 1048576;
    };
    loader = {
      grub.enable = false;
      generic-extlinux-compatible.enable = false;
      # This requires the top-level import kboot-conf
      kboot-conf = {
        enable = true;
        configurationLimit = 5;
      };
    };
    kernelParams = [
      "console=ttyAML0,115200n8"
      "consoleblank=0"
      "fsck.fix=yes"
      "fsck.repair=yes"
       # n2-plus
      "max_freq_a53=1908"
      "max_freq_a73=2208"
      "maxcpus=6"
    ];
    consoleLogLevel = lib.mkDefault 7;
    supportedFilesystems = lib.mkForce [ "vfat" "cifs" ];
    kernelPackages = pkgs.linuxPackages_latest;
  };

  # File systems configuration for using the installer's partition layout
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS";
      fsType = "ext4";
    };
  };

  system.stateVersion = "22.05";
}
