{ lib, config, pkgs, creds, mobile-nixos-path, ... }:

let
  mobile-nixos-configuration-path = mobile-nixos-path + "/lib/configuration.nix";
in {
  nxc.roles.mobile.enable = true;

  # ----- hardware
  imports = [
    (import mobile-nixos-configuration-path { device = "pine64-pinephone"; })
  ];

  boot = {
    initrd.availableKernelModules = [ ];
    initrd.kernelModules = [ ];
    kernelModules = [ ];
    extraModulePackages = [ ];
  };

  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SYSTEM";
      fsType = "ext4";
    };

    "/boot" = {
      device = "/dev/disk/by-label/mobile-nixos-boo";  # default label name too long ¯\_(ツ)_/¯
      fsType = "ext4";
    };
  };

  swapDevices = [ ];

  # -----

  # Prevent the ip change at each reboot...
  networking = {
    firewall.enable = false;
    useDHCP = false;
    interfaces.wlan0 = {
      useDHCP = false;  # turn this of true, if the following is not installing the right route
      ipv4 = {
        addresses = [{
          address = "192.168.150.50";
          prefixLength = 24;
        }];
        routes = [{
          # Configure the prefix route.
          address = "192.168.150.0";
          prefixLength = 24;
          via = "192.168.150.254";
        }];
      };
    };
    defaultGateway = "192.168.150.254";
    nameservers = [ "8.8.8.8" ];
  };

  systemd.services.sshd.wantedBy = lib.mkOverride 10 [ "multi-user.target" ];
  system.stateVersion = "21.03";
}
