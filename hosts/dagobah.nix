{ config, lib, pkgs, pytools, storage, creds, networks, hostname, ... }:

{
  nxc.roles.laptop.enable = true;

  # ----- vpn client part
  imports = [
    (
      import ../modules/openvpn-client.nix {
        inherit config pkgs creds;
        vpn-name = "work";
        device = "tun1";
        cipher = "BF-CBC";
        with-ta = false;
        with-passfile = false;
      }
    )
  ];

  networking = {
    enableB43Firmware = false;        # don't want those, they conflict with wl (and do not work)
    # head -c 8 /etc/machine-id
    hostId = "7e49e1c7";  # zfs related
  };

  # ----- hardware

  boot = {
    initrd.availableKernelModules = [ "xhci_hcd" "ehci_pci" "ahci" "usb_storage" "usbhid" ];
    # blacklist those wifi modules to avoid activating not working wifi card
    blacklistedKernelModules = [ "ath9k" "b43" "bcma" ];
    # wl module for wifi
    extraModulePackages = [ config.boot.kernelPackages.broadcom_sta ];
    # make those available from second stage of boot process
    kernelPackages = pkgs.linuxPackages_latest;
    kernelModules = [ "acpi-cpufreq" "kvm-intel" "wl" ];
    supportedFilesystems = [ "zfs" ];

    # To install dependencies on those filesystems
    initrd.supportedFilesystems = [ "vfat" "ntfs" "cifs" "nfs" ];

    # dropbox setting
    kernel.sysctl."fs.inotify.max_user_watches" = 1000000;

    loader = {
      # Use the systemd-boot EFI boot loader.
      systemd-boot.enable = true;
      efi = {
        canTouchEfiVariables = true;

        # assuming /boot is the mount point of the EFI partition in
        # NixOS (as the installation section recommends).
        efiSysMountPoint = "/boot";
      };
    };
  };

  fileSystems = {
    "/boot" = { fsType = "vfat"; device = "/dev/disk/by-label/boot"; neededForBoot = true; };
    "/"     = { fsType = "zfs";  device = "os/safe/slash";           neededForBoot= true; };
    "/nix"  = { fsType = "zfs";  device = "os/local/nix";            neededForBoot= true; options = [ "noatime" ]; };
    "/home" = { fsType = "zfs";  device = "rpool/home"; };
  };

  hardware.bluetooth.enable = false;

  # ----- common

  system.autoUpgrade.enable = false;

  system.activationScripts.bootstrap =
    ''
    if [ ! -d ${storage.volume} ]; then
      mkdir -m 0775 -p ${storage.volume}
      mkdir -m 0775 -p ${storage.share}
      chown -R root:users ${storage.volume}
    fi
    chown -R root:input /dev/uinput
    chmod 0660 /dev/uinput
  '';

  # Select internationalisation properties.
  console.font = "Lat2-Terminus16";
  console.keyMap = "us";
  i18n = {
    defaultLocale = "en_US.UTF-8";
    supportedLocales = [ "en_US.UTF-8/UTF-8" ];
  };

  fonts = {
    fontconfig.enable = true;
    # fontDir.enable = true;
    fonts = [
      pkgs.dejavu_fonts
      pkgs.powerline-fonts
    ];
  };

  # ----- sound

  # disable pc speaker audio card
  # blacklistedKernelModules = [ "snd_pcsp" ];

  boot.extraModprobeConfig = ''
    options snd slots=snd-hda-intel
    # disable first card and enable the second one
    # options snd_hda_intel enable=0,1
  '';

  # ----- networks

  networking = {
    firewall = {
      enable = true;
      allowedUDPPorts = [ 60501 ];
    };
  };


  # ----- nix

  nix.settings.max-jobs = 8;

  environment.etc = {
    "nix/signing.sec".text = creds.nixos-signing-key.myrkr.secret;
    "nix/signing.pub".text = creds.nixos-signing-key.myrkr.public;
  };

  nix = {
    extraOptions = ''
      keep-outputs = true
      keep-derivations = true
      keep-env-derivations = true
      builders-use-substitutes = true
      secret-key-files = /etc/nix/signing.sec
    '';
  };

  # ----- services

  # List services that you want to enable:
  services = {
    locate = {
      enable = true;
      interval = "00 19 * * *"; # update db at 19h every day
    };
  };
}
