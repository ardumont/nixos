{ lib, config, pkgs, nixpkgs, ... }:

{
  # Network configuration.
  networking.useDHCP = false;
  networking.firewall.allowedTCPPorts = [ 80 ];

  # Enable a web server.
  services.httpd = {
    enable = true;
    adminAddr = "morty@example.org";
  };
}
