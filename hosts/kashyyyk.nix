{ lib, config, pkgs, nixpkgs, creds, ... }:

{
  nxc.roles.laptop.enable = true;
  nxc.configs.media.enable = true;

  nix.settings.max-jobs = lib.mkDefault 12;

  networking = {
    firewall.enable = false;
    wireless = {
      enable = true;
      userControlled.enable = true;
      interfaces = [ "wlp1s0" ];
      networks = {
        tatooine = {
          pskRaw = creds.ssid.tatooine;
        };
      };
    };
  };

  # ----- hardware configuration

  services.xserver.videoDrivers = [ "amdgpu" ];
  boot = {
    initrd.availableKernelModules = [
      "nvme" "xhci_hcd" "usb_storage" "usbhid" "sd_mod"
    ];
    initrd.kernelModules = [ "kvm-amd" "amdgpu" ];
    # extra module to deal with wifi
    extraModulePackages = [
      # synced with the kernel version as the machine
      config.boot.kernelPackages.rtl8821ce
    ];
    # make those modules available at the second stage of boot process
    kernelModules = [
      # "wl"
    ];

    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
  };

  fileSystems = {
    "/" = { device = "/dev/disk/by-label/nixos"; fsType = "ext4"; };
    "/boot" = { device = "/dev/disk/by-label/boot"; fsType = "vfat"; };
  };

  swapDevices = [ { device = "/dev/disk/by-label/swap"; } ];

  system.stateVersion = "22.05";
}
