{ lib, config, pkgs, nixpkgs, ... }:

{
  nxc.roles.laptop-chris.enable = true;
  nix.settings.max-jobs = lib.mkDefault 8;

  networking.firewall.enable = false;

  # ----- hardware configuration

  hardware.enableRedistributableFirmware = true;

  boot = {
    initrd.availableKernelModules = [ "nvme" "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];
    # wl module for wifi
    extraModulePackages = [ config.boot.kernelPackages.rtl8821ce ];
    kernelModules = [ "kvm-amd" ];
    loader = {
      systemd-boot = {
        enable = true;
        consoleMode = "max";
        configurationLimit = 10;
      };
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot";
      };
    };
    # amd renoir is ok from 5.5 onward
    # kernelPackages = pkgs.linuxPackages_5_16;
    # kernelPackages = pkgs.linuxPackages_5_18;
    kernelPackages = pkgs.linuxPackages_latest;
  };

  fileSystems = {
    "/" = { device = "/dev/disk/by-label/nixos"; fsType = "ext4"; };
    "/boot" = { device = "/dev/disk/by-label/boot"; fsType = "vfat"; };
  };

  swapDevices = [ { device = "/dev/disk/by-label/swap"; } ];

  system.stateVersion = "22.05";
}
