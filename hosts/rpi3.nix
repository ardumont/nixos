{ pkgs, ... }:

{
  nxc.roles.server-light.enable = true;

  boot = {
    loader = {
      # NixOS wants to enable GRUB by default
      grub.enable = false;
      # Enables the generation of /boot/extlinux/extlinux.conf
      generic-extlinux-compatible.enable = true;
    };

    # Raspberry Pi 3
    kernelPackages = pkgs.linuxPackages_latest;

    # Needed for the virtual console to work on the RPi 3, as the
    # default of 16M doesn't seem to be enough.  If X.org behaves
    # weirdly, then try increasing this to 256M.
    kernelParams = [
      "cma=32M"                 # graphical issue
      "console=ttyS1,115200n8"  # serial console
    ];

    kernel.sysctl."fs.inotify.max_user_watches" = 1000000;
  };

  # File systems configuration for using the installer's partition layout
  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-label/NIXOS_BOOT";
      fsType = "vfat";
    };
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };
}
