# old odroid-n2
{ config, lib, pkgs, hostname, home-manager, identity, ... }:

{
  nxc.roles.server.enable = true;

  # Remaining services to port:
  # imports = map (m:
  #   assert builtins.isPath m;
  #   import m { inherit config lib pkgs hostname home-manager identity; } )
  #   [
  #     ../modules/home/manager.nix
  #   ];

  # head -c 8 /etc/machine-id
  # networking.hostId = "c4e7b98b";

  powerManagement.cpuFreqGovernor = lib.mkDefault "ondemand";
  # for python3.9-tvdb_api-3.1.0
  nixpkgs.config.allowBroken = true;

  hardware.deviceTree.name = "amlogic/meson-g12b-odroid-n2.dtb";
  boot = {
    kernel.sysctl = {
      "net.core.rmem_max" = 4194304;
      "net.core.wmem_max" = 1048576;
      "fs.inotify.max_user_watches" = 1048576;
    };
    loader = {
      grub.enable = false;
      generic-extlinux-compatible.enable = false;
      # This requires the top-level import kboot-conf
      kboot-conf = {
        enable = true;
        configurationLimit = 5;
      };
    };
    kernelParams = [
      "console=ttyAML0,115200n8"
      "consoleblank=0"
      "fsck.fix=yes"
      "fsck.repair=yes"
      # n2
      "max_freq_a53=1896"
      "max_freq_a73=1800"
      # n2
      "max_freq_a53=1896"
      "max_freq_a73=1800"
      "maxcpus=6"
    ];
    consoleLogLevel = lib.mkDefault 7;
    supportedFilesystems = lib.mkForce [ "vfat" "cifs" ];
    kernelPackages = pkgs.linuxPackages_latest;
  };

  # File systems configuration for using the installer's partition layout
  fileSystems = {
    "/" = {
      device = "/dev/disk/by-label/NIXOS_SD";
      fsType = "ext4";
    };
  };
}
