{ lib, config, ... }:

with lib;
let cfg = config.nxc.roles.server;
in {
  options = {
    nxc.roles.server = {
      enable = mkEnableOption "Server (headless) configuration";
    };
  };

  config = mkIf cfg.enable {
    nxc.configs = {
      headless.enable = true;
      dns-server.enable = true;
      minidlna.enable = true;
      redis.enable = true;
      syncthing.enable = true;
      weechat.enable = true;
      nfs-server.enable = true;
      mounts-server.enable = true;
      build-concurrency-light.enable = true;
      transmission.enable = true;
      # pytools-services.enable = true;
      mediatomb.enable = false;
      packages.enable = true;
      admin-tools.enable = true;
      wireguard-server.enable = true;
      nix-allow-broken.enable = false;
      metrics-server.enable = true;
      metrics-exporter.enable = true;
    };
  };
}
