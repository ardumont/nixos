{ lib, config, ... }:

{
  imports = [
    ./base.nix
    ./laptop.nix
    ./laptop-chris.nix
    ./desktop.nix
    ./server.nix
    ./server-light.nix
    ./mobile.nix
  ];

  # All roles share the same base role
  config.nxc.roles.base.enable = lib.mkDefault true;
}
