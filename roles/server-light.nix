{ lib, config, ... }:

with lib;
let cfg = config.nxc.roles.server-light;
in {
  options = {
    nxc.roles.server-light = {
      enable = mkEnableOption "Light Server (headless) configuration";
    };
  };

  config = mkIf cfg.enable {
    nxc.configs = {
      headless.enable = true;
      aliases.enable = true;
      wpa-supplicant.enable = true;  # most raspberry pis do have the wifi
      swap.enable = true;
      packages.enable = true;
      admin-tools.enable = true;
    };
  };
}
