{ lib, config, ... }:

with lib;
let cfg = config.nxc.roles.mobile;
in {
  options = {
    nxc.roles.mobile = {
      enable = mkEnableOption "Mobile phone configuration";
    };
  };

  config = mkIf cfg.enable {
    nxc.configs = {
      zfs.enable = false;
      mobile.enable = true;
      wpa-supplicant.enable = true;
      xserver-phosh.enable = true;
      build-concurrency-light.enable = true;
    };
  };
}
