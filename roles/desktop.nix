{ lib, config, ... }:

with lib;
let cfg = config.nxc.roles.desktop;
in {
  options = {
    nxc.roles.desktop = {
      enable = mkEnableOption "Desktop configuration";
    };
  };

  config = mkIf cfg.enable {
    nxc.configs = {
      xserver-basic.enable = true;
      # udev.enable = true;
      # mounts-client.enable = true;
      dnsmasq-client.enable = true;
      gc.enable = true;
      swap.enable = true;
      # pytools-convert.enable = true;
      # home-manager.enable = false;
      admin-tools.enable = true;
      packages.enable = true;
      wireguard-client.enable = true;
      wpa-supplicant.enable = true;
      zfs.enable = true;
      media.enable = true;
    };
  };
}
