{ config, nixpkgs, lib, pkgs, ... }:

with lib;
let
  cfg = config.nxc.roles.base;
in
{
  options.nxc.roles.base.enable = mkEnableOption "Base configuration";

  imports = [ nixpkgs.nixosModules.notDetected ];

  config = mkIf cfg.enable {
    nxc.configs = {
      packages.enable = true;
      users.enable = true;
      nix.enable = true;
      openssh.enable = true;
      tmux.enable = true;
    };
  };
}
