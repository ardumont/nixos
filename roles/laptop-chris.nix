{ lib, config, ... }:

with lib;
let cfg = config.nxc.roles.laptop-chris;
in {
  options = {
    nxc.roles.laptop-chris = {
      enable = mkEnableOption "Chris' laptop(s) configuration";
    };
  };

  config = mkIf cfg.enable {
    nxc.configs = {
      laptop.enable = true;
      laptop-lid-close.enable = true;
      sound.enable = true;
      xserver-default.enable = true;
      printer.enable = true;
      wpa-supplicant.enable = true;
      udev.enable = true;
      # home-manager.enable = false;
      admin-tools.enable = true;
      packages.enable = true;
      wireguard-client.enable = true;
      dnsmasq-client.enable = true;
      gc.enable = true;
    };
  };
}
