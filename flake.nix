{
  description = "nixos configuration, the flake way";

  inputs = {
    nixpkgs = {
      type = "github";
      owner = "NixOS";
      repo = "nixpkgs";
      ref = "nixos-unstable";
    };

    pkgs-ard-mediatomb = {
      type = "github";
      owner = "ardumont";
      repo = "nixpkgs";
      ref = "mediatomb-custom-import";
    };

    pytools = {
      type = "gitlab";
      owner = "ardumont";
      repo = "pytools";
      ref = "master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    mypkgs = {
      type = "github";
      owner = "ardumont";
      repo = "mypkgs";
      ref = "master";
    };

    home-manager = {
      type = "github";
      owner = "nix-community";
      repo = "home-manager";
      ref = "master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    rpi4-kernel = {
      type = "github";
      owner = "NixOS";
      repo = "nixpkgs";
      ref = "95005c103926d2e2547b9410c75e51b6d4c218bb";
      flake = false;
    };

    mobile-nixos = {
      type = "github";
      owner = "NixOS";
      repo = "mobile-nixos";
      ref = "master";
      flake = false;
    };

    private-data = {
      url = "/etc/private-data";
    };
  };

  outputs = {
    self, nixpkgs, pytools, mypkgs, home-manager, pkgs-ard-mediatomb,
    rpi4-kernel, mobile-nixos, private-data, ...
  } @ inputs:
    let
      lib = nixpkgs.lib;
      creds = import private-data { inherit lib; };
      identity = creds.identity;
      media = creds.media;
      storage = import ./data/storage.nix;
      networks = (import ./data/networks.nix { inherit lib; });
      lib-helper = (import ./modules/lib-helper.nix { inherit storage; });
      mobile-nixos-path = mobile-nixos.outPath;
      forEachSystem = lib.genAttrs [ "x86_64-linux" "aarch64-linux" ];
      pkgsBySystem = forEachSystem (system:
        let mypkgs-realized = (import inputs.mypkgs {
              inherit lib;
              pkgs = (import inputs.nixpkgs { inherit system; }).pkgs;
            });
        in import inputs.nixpkgs {
          inherit system;
          overlays = [ (self: super: {
            weechat = super.weechat.override {
              configure = { ...}: {
                scripts = [
                  # mypkgs-realized.wee-most
                  mypkgs-realized.wee-matter
                ];
              };
            };
          })];
        }
      );
      mkNixOsConfiguration = hostname: { system, config-path, container ? false }:
        lib.nameValuePair hostname (lib.nixosSystem {
          inherit system;

          modules = [
            ({ ... }: {
              programs.ssh.startAgent = false; # do not start agent
              time.timeZone = "Europe/Paris";
              # is this a container, by default, no
              boot.isContainer = container;
            })
            ({ hostname, ... }: {
              networking.hostName = hostname;
            })
            ({ inputs, ... }: {
              # Use the nixpkgs from the flake.
              nixpkgs = {
                pkgs = pkgsBySystem."${system}";
              };

              # For compatibility with nix-shell, nix-build, etc.
              environment.etc.nixpkgs.source = inputs.nixpkgs;
            })
            ({ pkgs, ... }: {
              # Enable a flake-compatible version of Nix.
              nix = {
                package = pkgs.nixFlakes;
                extraOptions = ''
                experimental-features = nix-command flakes
                '';
              };
            })
            ({ lib, ... }: {
              # Set the system configuration revision (`nixos-version --json` shows the
              # git revision of the flake)
              system.configurationRevision = lib.mkIf (self ? rev) self.rev;
            })
            (import ./configs)
            (import ./roles/default.nix)
            (import config-path)
          ];
          specialArgs = {
            inherit nixpkgs hostname inputs storage networks creds
              pytools media pkgs-ard-mediatomb rpi4-kernel
              lib-helper system mobile-nixos-path
              mypkgs home-manager identity;
          };
        });
    in {
      # Attribute set of hostnames to evaluate NixOS configurations.
      nixosConfigurations = lib.mapAttrs' mkNixOsConfiguration {
        alderaan  = { system = "x86_64-linux";  config-path = ./hosts/alderaan.nix; };
        myrkr     = { system = "x86_64-linux";  config-path = ./hosts/myrkr.nix; };
        dathomir  = { system = "x86_64-linux";  config-path = ./hosts/dathomir.nix; };
        kashyyyk  = { system = "x86_64-linux";  config-path = ./hosts/kashyyyk.nix; };
        container = { system = "x86_64-linux";  config-path = ./hosts/container.nix; container = true; };
        odroid-n2 = { system = "aarch64-linux"; config-path = ./hosts/odroid-n2.nix; };
        odroid    = { system = "aarch64-linux"; config-path = ./hosts/odroid.nix; };
        rpi3      = { system = "aarch64-linux"; config-path = ./hosts/rpi3.nix; };
        rpi4      = { system = "aarch64-linux"; config-path = ./hosts/rpi4.nix; };
        pinephone = { system = "aarch64-linux"; config-path = ./hosts/pinephone.nix; };
      };

      devShell = forEachSystem (system:
        let pkgs = pkgsBySystem.${system}; in
        import ./shell.nix { inherit pkgs; }
      );
    };
}
